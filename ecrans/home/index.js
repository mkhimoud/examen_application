import { View, Text, ScrollView, Image, FlatList, TouchableOpacity } from 'react-native'
import React from 'react'

import dashboardStyles from './style.js';
import { FakeActivity } from '../../fakeData/fackActivity';
import ActivityItem from '../../composantes/ActivityItem';
import PanneItem    from '../../composantes/panneItem';
import { fakePanne } from '../../fakeData/fakePanne.js';
import { fakeMecanicien } from '../../fakeData/fakeMecanicien.js';




const Home = () => {
  return (
    <ScrollView>
      <View style={dashboardStyles.header}>
        <Text style={dashboardStyles.userName}>Massinissa</Text>
        <Image source={require('./../../Assets/20494859.webp')}  style={dashboardStyles.userImg} />
      </View>

      <FlatList data={FakeActivity}
       keyExtractor={item => item.id}
       horizontal={true}
       showsHorizontalScrollIndicator={false}
       style={dashboardStyles.scrollablelist}
       renderItem={({item}) =>{
        return (
         <ActivityItem item={item}/>
        )
       }
       }/>
      
      <View style={dashboardStyles.title}>
        <Text style={dashboardStyles.titleStyle}>
          Quel panne avez vous ?
        </Text>
      </View>
      <FlatList data={fakePanne}
       keyExtractor={item => item.id}
       horizontal={true}
       showsHorizontalScrollIndicator={false}
       style={dashboardStyles.scrollablelist}
       renderItem={({item}) =>{
        return (
         <PanneItem item={item}/>
        )
       }
       }/>


      <View style={dashboardStyles.title_mecanicien}>
        <Text style={dashboardStyles.titleStyle}>
          Trouver un mecanicien ?
        </Text>
        <TouchableOpacity style={dashboardStyles.link}>
          <Text>Afficher tout</Text>
        </TouchableOpacity>
      </View>
      
      <View style={dashboardStyles.mecanicienContainer}>
        {fakeMecanicien.map((mecanicien, index) => {
          return (
          <TouchableOpacity key={mecanicien.id} style={dashboardStyles.mecanicienCard}>
            <Image source={{uri: `${mecanicien.img}`}} style={dashboardStyles.mecanicienImage}/>
            <View style={dashboardStyles.mecanicienInfo}>
            <Text>{mecanicien.fullname}</Text>
            <Text>{mecanicien.speciality}</Text>
            </View>
          </TouchableOpacity>)
        })}

      </View>
    </ScrollView>
  );
};

export default Home;
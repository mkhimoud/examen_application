import { StyleSheet } from "react-native"
// import { white } from "react-native-paper/lib/typescript/styles/themes/v2/colors";
import { PADDING } from "../../outils/contantes";
import { COLORS } from "../../outils/contantes";

const dashboardStyles = StyleSheet.create({
    header:  {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: PADDING.horizontal,
        paddingVertical: PADDING.vertical,
        backgroundColor: 'white',
    },
    userImg:{
        width: 40,
        height: 40,
        borderRadius: 20,
    },
    // userName: {
    //     fontsize: 2,
    // },
    scrollablelist:{
        paddingHorizontal: PADDING.horizontal,
        paddingVertical: PADDING.vertical,
    },
    title:{
        paddingHorizontal: PADDING.horizontal,
        paddingVertical: PADDING.vertical,
    },
    titleStyle:{
        fontWeight:'bold',
        fontSize:15,
    },
    title_mecanicien:{
        paddingHorizontal: PADDING.horizontal,
        paddingVertical: PADDING.vertical,
        flexDirection:'row',
        justifyContent:'space-between',
        paddingTop:5,
    },
    link:{
        color: COLORS.main,
    },
    mecanicienContainer:{
        paddingHorizontal: PADDING.horizontal,
        paddingVertical: PADDING.vertical,
    },
    mecanicienCard:{
      flexDirection:'row',
      backgroundColor:'white',
      flex:1,
      elevation:5,
      paddingHorizontal: PADDING.horizontal,
        paddingVertical: PADDING.vertical,
        padding:15,
        marginBottom:10,

    },
    mecanicienImage:{
        height:90,
        width:90,
        borderRadius:50,
        marginRight:20,
    },
    mecanicienInfo:{
       flexDirection:'column', 
    }
});

export default dashboardStyles;
import { View, Text, Image, TouchableOpacity } from 'react-native';
import React from 'react';
import styles from './style'


const PanneItem = ({item}) => {
  return (
    <TouchableOpacity>
      <View  style={styles.item}>
        <Image style={styles.itemImage} source={require(`./../../Assets/images/panne-qashqai.jpg`)} />
      <Text>{item.libelle}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default PanneItem;
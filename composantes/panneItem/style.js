import { StyleSheet } from "react-native";
import { PADDING } from "../../outils/contantes";


const styles = StyleSheet.create({
item:{
flexDirection: 'row',
margin:10,
paddingHorizontal: PADDING.horizontal,
paddingVertical: PADDING.vertical,
backgroundColor: 'white',
alignItems: 'center',

},
itemImage:{
    width: 80,
    height: 80,
    marginRight:10,
}

})

export default styles
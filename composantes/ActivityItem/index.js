import { View, Text, TouchableOpacity } from 'react-native';
import React from 'react';
import SVG_MECANICS  from './../../Assets/images/svg/undraw_automobile_repair_ywci.svg';
import styles from './style';
const ActivityItem = ({item}) => {
  return (
    <TouchableOpacity style={styles.scrollableListItem}>
            <SVG_MECANICS width={60} height={50} />
          <Text style={styles.mainText} >{item.mainText}</Text>
          <Text style={styles.subText}>{item.subText}</Text>
    </TouchableOpacity>
  )
}

export default ActivityItem;
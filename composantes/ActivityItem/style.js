import { StyleSheet } from "react-native";

 const styles = StyleSheet.create({
    scrollableListItem:{
        flexDirection: 'column',
        paddingHorizontal:15,
        paddingVertical:15,
        backgroundColor: 'white',
        marginRight:16,
        elevation: 15,
    },
    mainText:{
        fontWeight: 'bold',
        fontSize: 20,
    },
    subText:{
        
        marginTop: 8,
        fontSize: 15,
    },
})
export default styles;